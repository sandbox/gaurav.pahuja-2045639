
-- SUMMARY --

Allows administrator to check for available Opcode caching mechanism on the server.

-- REQUIREMENTS --

NA


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


Current maintainers:
* Gaurav Pahuja (gaurav.pahuja) - https://drupal.org/user/1804170

This project has been sponsored by:
* Sapient Consulting

